﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using SampleSheetModel;

namespace sampleSheetAutomation
{
   public class SampleConvertStyle : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                SampleSheetData sampleSheetData = new SampleSheetData() ;
                sampleSheetData.SampleNameLegal = (bool)value[0];
                sampleSheetData.SampleNameRepeat = (bool)value[1];
                if (!sampleSheetData.SampleNameLegal) {
                    return "#871F78";
                }
                if (sampleSheetData.SampleNameRepeat == true) {
                    return "#D9D9F3";
                }
                return parameter;
            }
            catch (Exception)
            {
                return parameter;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
      
    }
}
