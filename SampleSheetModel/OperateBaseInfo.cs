﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSheetModel
{
   public class OperateBaseInfo : INotifyPropertyChanged
    {   
        /// <summary>
        /// 操作人
        /// </summary>
        private string _operater;
        public string Operater {
            get { return _operater; }
            set{
                if (_operater != value)
                {
                    _operater = value;
                    OnPropertyChanged("Operater");
                }
            }
        }

        /// <summary>
        /// 操作时间
        /// </summary>
        private string _operateDate;
        public string OperateDate
        {
            get { return _operateDate; }
            set
            {
                if (_operateDate != value)
                {
                    _operateDate = value;
                    OnPropertyChanged("OperateDate");
                }
            }
        }
        /// <summary>
        /// 操作时间
        /// </summary>
      
        public string OperateDate1
        {
            get{
                if (_operateDate != null) {

                    return Convert.ToDateTime(_operateDate).ToString("yyyyMMdd");
                }
                return "";
            }
        }

        /// <summary>
        /// 试剂盒类型
        /// </summary>
        private string _kitType;
        public string KitType
        {
            get { return _kitType; }
            set
            {
                if (_kitType != value)
                {
                    _kitType = value;
                    OnPropertyChanged("KitType");
                }
            }
        }
        /// <summary>
        /// 标签类型
        /// </summary>
        private string _markTypes;
        public string MarkTypes
        {
            get { return _markTypes; }
            set
            {
                if (_markTypes != value)
                {
                    _markTypes = value;
                    OnPropertyChanged("MarkTypes");
                }
            }
        }
        /// <summary>
        /// 测序读长
        /// </summary>
        private string _readLength;
        public string ReadLength
        {
            get { return _readLength; }
            set
            {
                if (_readLength != value)
                {
                    _readLength = value;
                    OnPropertyChanged("ReadLength");
                }
            }
        }

      

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
