﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSheetModel
{
    public class BaseDic : INotifyPropertyChanged
    {
     
        private string _itemKey;
        public string ItemKey
        {
            get { return _itemKey; }
            set
            {
                if (_itemKey != value)
                {
                    _itemKey = value;
                    OnPropertyChanged("ItemKey");
                }
            }
        }
        private string _itemName;
        public string ItemName
        {
            get { return _itemName; }
            set
            {
                if (_itemName != value)
                {
                    _itemName = value;
                    OnPropertyChanged("ItemName");
                }
            }
        }

        /// <summary>
        /// 字典类型
        /// </summary>
        private string _itemType;
        public string ItemType
        {
            get { return _itemType; }
            set
            {
                if (_itemType != value)
                {
                    _itemType = value;
                    OnPropertyChanged("ItemType");
                }
            }
        }

        public ObservableCollection<BaseDic> GetDictionaryByName(string path,string dicName) {
            ObservableCollection<BaseDic> dics = new ObservableCollection<BaseDic>();
            string[][] excelData = getExcelRows(path, dicName);
            foreach (string[] row in excelData)
            {
                if (string.IsNullOrWhiteSpace(row[0]))
                    continue;
                else
                {
                    BaseDic baseDic = new BaseDic();
                    baseDic.ItemKey = row[0];
                    baseDic.ItemName = row[1];
                    dics.Add(baseDic);
                }
            }
            return dics;
        }


        public List<BaseDic> GetDictionars(string path)
        {
            List<BaseDic> dics = new List<BaseDic>();
            string[][] excelData = getExcelRows(path, "combine");
            foreach (string[] row in excelData)
            {
                if (string.IsNullOrWhiteSpace(row[0]))
                    continue;
                else
                {
                    BaseDic baseDic = new BaseDic();
                    baseDic.ItemKey = row[0];
                    baseDic.ItemName = row[1];
                    baseDic.ItemType = row[2];
                    dics.Add(baseDic);
                }
            }
            return dics;
        }

        private string[][] getExcelRows(string path, string dicName) {
            string[][] row;
            using (ExcelProcessor excel = new ExcelProcessor())
            {
                int sheetNum = 1;
                switch (dicName.ToLower())
                {
                    case "kittype": sheetNum = 1; break;
                    case "readlength": sheetNum = 2; break;
                    case "marktyoe": sheetNum = 3; break;
                    case "probetypes": sheetNum = 4; break;
                    case "combine": sheetNum = 5; break;
                    default: sheetNum = 1; break;
                }
                
                row = excel.ParseExcelSheet(path, sheetNum);
            }
            return row;

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
