﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SampleSheetModel;

namespace sampleSheetAutomation
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private ReferenceInfo referenceInfo = new ReferenceInfo();
        private Key preKey;
        private string OutputFilePath = "";
        public MainWindow()
        {
            InitializeComponent();
            ContentRendered += MainWindow_Rendered;
            ContentRendered += MainWindow_SizeChanged;
            SizeChanged += MainWindow_SizeChanged;
        }
        private void MainWindow_Rendered(object sender, EventArgs e) {
            BaseInfoGrid.DataContext = referenceInfo.operateBaseInfo;
            SampleSheetDataGrid.ItemsSource = referenceInfo.SampleSheetDatas;
            DataObject.AddPastingHandler(SampleSheetDataGrid,OnRefTextBoxPaste);
            DataObject.AddPastingHandler(SampleSheetDataGrid, OnRefTextBoxPaste);
        }

        private void MainWindow_SizeChanged(object sender, EventArgs e)
        {
            Canvas.SetLeft(titleLabel, (ActualWidth - titleLabel.ActualWidth) / 2);
            LayOutGrid.Width = ActualWidth * .95;
            LayOutGrid.Height = ActualHeight * .8;
            LayOutGrid.Margin = new Thickness(0, ActualHeight / 20, 0, 0);


            BaseInfoGrid.Width = LayOutGrid.Width;
            BaseInfoGrid.Height = LayOutGrid.Height * 4 / 11;

            SampleSheetDataGrid.Width = LayOutGrid.Width;
            SampleSheetDataGrid.Height = LayOutGrid.Height * 6 / 11;

            OperateGrid.Width = LayOutGrid.Width;
            OperateGrid.Height = LayOutGrid.Height * 1 / 11;
        }
        private void OnRefTextBoxPaste(object sender, DataObjectPastingEventArgs e)
        {
            List<List<string>> newCqInputs = new List<List<string>>();
            foreach (string line in Regex.Split(Clipboard.GetText(), "\r\n|\r|\n").Where(x => !string.IsNullOrEmpty(x)))
            {
                List<string> c = new List<string>();
                foreach (string s in line.Split('\t').ToList())
                {
                    c.Add(s);
                }
                newCqInputs.Add(c);
            }
            DataGrid dg = sender as DataGrid;
            int tbCol = dg.CurrentColumn.DisplayIndex;
            int tbRow = dg.SelectedIndex;
            int collength = Math.Min(3-tbCol,newCqInputs[0].Count);
            for (int row = 0; row < newCqInputs.Count; row++) {
                if ((row + tbRow + 1) > referenceInfo.SampleSheetDatas.Count) {
                    referenceInfo.SampleSheetDatas.Add(new SampleSheetData());
                }
                SampleSheetData sampleSheetData= referenceInfo.SampleSheetDatas[row + tbRow];
                if (tbCol == 0) {
                    sampleSheetData.SampleName = newCqInputs[row][0];
                    if (collength > 1) {
                        sampleSheetData.IndexName = newCqInputs[row][1];
                    }
                    if (collength > 2)
                    {
                        sampleSheetData.ProbeTypes = newCqInputs[row][2];
                    }

                }
                if (tbCol == 1)
                {
                    sampleSheetData.IndexName = newCqInputs[row][0];
                    if (collength > 1)
                    {
                        sampleSheetData.ProbeTypes = newCqInputs[row][1];
                    }
                }
                if (tbCol == 2)
                {
                    sampleSheetData.ProbeTypes = newCqInputs[row][0];
                }

            }
            e.CancelCommand();
        }
        private void ImportExcel_Click(object sender, RoutedEventArgs e)
        {
         
            var fileDialog = new System.Windows.Forms.OpenFileDialog();
            fileDialog.DefaultExt = ".xlsx";
            fileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx";
            var result = fileDialog.ShowDialog();
            string FileName = "";
            switch (result)
            {
                case System.Windows.Forms.DialogResult.OK:
                    FileName = fileDialog.FileName;
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                    break;
                default:
                    FileName = "";
                    break;
            }
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                ExcelHelper excelHelper = new ExcelHelper();
                ImportExcel.IsEnabled = false;
                new Task((ImportExcel) => {
                    referenceInfo.SampleSheetDatas = excelHelper.ImportSampleSheetData(FileName);
                    this.SampleSheetDataGrid.Dispatcher.Invoke(new Action(()=> {
                        this.SampleSheetDataGrid.ItemsSource = referenceInfo.SampleSheetDatas;
                    }));
                    Button button = ImportExcel as Button;
                    Action action1 = () =>
                    {
                        button.IsEnabled = true;
                    };
                    button.Dispatcher.BeginInvoke(action1);
                }, ImportExcel).Start();
             
            }
           
        }

        private void GenernateSampleSheet_Click(object sender, RoutedEventArgs e)
        {
            GenernateSampleSheet.IsEnabled = false;


            //var task= new Task((GenernateSampleSheet) => {

            //}, GenernateSampleSheet);

            Thread importThread = new Thread(new ThreadStart(Export));
            importThread.SetApartmentState(ApartmentState.STA); //重点
            importThread.Start();


        }
        public void Export() {
            if (CheckSampleSheet())
            {
                var folderDialog = new System.Windows.Forms.FolderBrowserDialog();
                var result = folderDialog.ShowDialog();
                OutputFilePath = "";
                //GenernateSampleSheet.IsEnabled = false;
                switch (result)
                {
                    case System.Windows.Forms.DialogResult.OK:
                        OutputFilePath = folderDialog.SelectedPath;
                        if (OutputFilePath.EndsWith("\\"))
                            OutputFilePath += "\\";
                        //GenernateSampleSheet.IsEnabled = false;
                        break;
                    case System.Windows.Forms.DialogResult.Cancel:
                        // GenernateSampleSheet.IsEnabled = true;
                        break;
                    default:
                        OutputFilePath = "";
                        // GenernateSampleSheet.IsEnabled = true;
                        break;
                }
                if (!string.IsNullOrEmpty(OutputFilePath))
                {
                    // GenernateSampleSheet.IsEnabled = false;
                    ExcelHelper excelHelper = new ExcelHelper();
                    excelHelper.ExportSampleSheetData(referenceInfo, referenceInfo.SampleSheetDatas, OutputFilePath);
                    // GenernateSampleSheet.IsEnabled = true;

                }

            }
            Button button = GenernateSampleSheet as Button;
            Action action1 = () =>
            {
                button.IsEnabled = true;
            };
            button.Dispatcher.BeginInvoke(action1);

        }
        private void OpeanFile_Click(object sender, RoutedEventArgs e)
        {
            //System.Diagnostics.Process.Start("ExpLore", OutputFilePath);
            referenceInfo.SampleSheetDatas = new ObservableCollection<SampleSheetData> {new SampleSheetData()};
            SampleSheetDataGrid.ItemsSource = referenceInfo.SampleSheetDatas;
        }

        private void SampleSheetDataGrid_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {

        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            //SampleSheetData sampleSheetData=  button.DataContext  as SampleSheetData;
            //int index= referenceInfo.SampleSheetDatas.IndexOf(sampleSheetData);
            //referenceInfo.SampleSheetDatas.Insert(index+1,new SampleSheetData());
            referenceInfo.SampleSheetDatas.Insert(referenceInfo.SampleSheetDatas.Count, new SampleSheetData());
            SampleSheetDataGrid.Items.MoveCurrentToLast();
            SampleSheetDataGrid.CurrentItem = SampleSheetDataGrid.Items[SampleSheetDataGrid.Items.Count-1];
            SampleSheetDataGrid.ScrollIntoView(SampleSheetDataGrid.CurrentItem);


        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            SampleSheetData sampleSheetData = button.DataContext as SampleSheetData;
            referenceInfo.SampleSheetDatas.Remove(sampleSheetData);
            if (referenceInfo.SampleSheetDatas.Count == 0) {
                referenceInfo.SampleSheetDatas.Add(new SampleSheetData());
            }
        }
        private bool CheckSampleSheet() {

            var nameRepeat = referenceInfo.SampleSheetDatas.GroupBy(x => x.SampleName).Where(x => x.Count() > 1).ToList();
            var indexRepeat = referenceInfo.SampleSheetDatas.GroupBy(x => x.IndexName).Where(x => x.Count() > 1).ToList();
            string path = System.Windows.Forms.Application.StartupPath;
            string templatPath = path + @".\excel\indexInfo.xlsx";
            IndexInfo indexInfo = new IndexInfo();
            List<IndexInfo> indexInfos;
            if (String.IsNullOrEmpty(referenceInfo.operateBaseInfo.Operater))
            {
                MessageBox.Show("实验人不可以为空");
                return false;
            }
            if (String.IsNullOrEmpty(referenceInfo.operateBaseInfo.OperateDate))
            {
                MessageBox.Show("实验日期不可以为空");
                return false;
            }
            if (String.IsNullOrEmpty(referenceInfo.operateBaseInfo.KitType))
            {
                MessageBox.Show("试剂盒类型不可为空");
                return false;
            }
            if (String.IsNullOrEmpty(referenceInfo.operateBaseInfo.MarkTypes))
            {
                MessageBox.Show("标签类型不可为空");
                return false;
            }
            if (String.IsNullOrEmpty(referenceInfo.operateBaseInfo.ReadLength))
            {
                MessageBox.Show("读长不可为空");
                return false;
            }
            int length = 0;
            if (referenceInfo.operateBaseInfo.KitType.Contains("Miniseq"))
            {
                length = 300;
            }
            else {
                string[] arr = referenceInfo.operateBaseInfo.KitType.Split(' ');
                string result = arr.Length > 2 ? arr[2] : "";
                string[] arr1 = result.Split('-');
                length = Convert.ToInt32(arr1[0]);
            }
            int  maxlength = Convert.ToInt32(length) / 2 + 1;
            int readLength = Convert.ToInt32(referenceInfo.operateBaseInfo.ReadLength);
            if (readLength> maxlength) {
                MessageBox.Show("试剂盒类型与测序读长不符！");
                return false;
            }
            if (referenceInfo.operateBaseInfo.MarkTypes == "单")
            {
                indexInfos = indexInfo.GetDictionaryByName(templatPath, "single").ToList();
            }
            else
            {
                indexInfos = indexInfo.GetDictionaryByName(templatPath, "double").ToList();
            }
            foreach (SampleSheetData sampleS in referenceInfo.SampleSheetDatas)
            {
                sampleS.SampleNameRepeat = nameRepeat.FindAll(x => x.Key == sampleS.SampleName).Count > 0;
                sampleS.IndexNameLegal = indexRepeat.FindAll(x => x.Key == sampleS.IndexName).Count > 0;
                sampleS.IndexNameExit = indexInfos.FindAll(x => x.IndexName == sampleS.IndexName).Count > 0;
            }
            if (referenceInfo.SampleSheetDatas.ToList().FindAll(x => x.SampleNameLegal == false).Count > 0)
            {
                MessageBox.Show("存在样本命名不合法");
                return false;
            }
            if (referenceInfo.SampleSheetDatas.ToList().FindAll(x => x.SampleNameRepeat == true).Count > 0)
            {
                MessageBox.Show("样本名重复");
                return false;
            }
            if (referenceInfo.SampleSheetDatas.ToList().FindAll(x => x.IndexNameLegal == true).Count > 0)
            {
                MessageBox.Show("存在index冲突");
                return false;
            }
            if (referenceInfo.SampleSheetDatas.ToList().FindAll(x => x.IndexNameExit == false).Count > 0)
            {
                MessageBox.Show("标签类型与接头名称不符！");
                return false;
            }
           
            return true;

        }
        private void CheckoutBtn_Click(object sender, RoutedEventArgs e)
        {
            CheckoutBtn.IsEnabled = false;
            new Task((CheckoutBtn) => {
                CheckSampleSheet();
                Button button = CheckoutBtn as Button;
                Action action1 = () =>
                {
                    button.IsEnabled = true;
                };
                button.Dispatcher.BeginInvoke(action1);
            }, CheckoutBtn).Start();
        }

        private void DatePicker_MouseLeave(object sender, MouseEventArgs e)
        {
            DatePicker picker = sender as DatePicker;
            try
            {
                DateTime dt;
                if (picker.Text.Length == 8) {
                     dt = DateTime.ParseExact(picker.Text, "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture);
                    picker.Text = dt.ToString("yyyy/MM/dd"); ;
                    return;
                }
                if (picker.Text.Contains("-")) {
                    dt = DateTime.ParseExact(picker.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.CurrentCulture);
                    picker.Text = dt.ToString("yyyy/MM/dd"); 
                    return;
                }
                
            }
            catch (Exception ex) {
               
            }

            
        }

        private void Probes_KeyDown(object sender, KeyEventArgs e)
        {

            if ((e.KeyboardDevice.IsKeyDown(Key.LeftCtrl) || e.KeyboardDevice.IsKeyDown(Key.RightCtrl)) && e.KeyboardDevice.IsKeyDown(Key.V))
            {

                List<List<string>> newCqInputs = new List<List<string>>();
                foreach (string line in Regex.Split(Clipboard.GetText(), "\r\n|\r|\n").Where(x => !string.IsNullOrEmpty(x)))
                {
                    List<string> c = new List<string>();
                    foreach (string s in line.Split('\t').ToList())
                    {
                        c.Add(s);
                    }
                    newCqInputs.Add(c);
                }
                DataGrid dg = SampleSheetDataGrid as DataGrid;
                int tbCol = dg.CurrentColumn.DisplayIndex;
                int tbRow = dg.SelectedIndex;
                for (int row = 0; row < newCqInputs.Count; row++)
                {
                    if ((row + tbRow + 1) > referenceInfo.SampleSheetDatas.Count)
                    {
                        referenceInfo.SampleSheetDatas.Add(new SampleSheetData());
                    }
                    SampleSheetData sampleSheetData = referenceInfo.SampleSheetDatas[row + tbRow];
                    sampleSheetData.ProbeTypes = newCqInputs[row][0];
                }
            }
        }
    }
}
