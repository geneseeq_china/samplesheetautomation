﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using SampleSheetModel;

namespace sampleSheetAutomation
{
   public class IndexConvertStyle : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
           
                if((bool)value== true)
                {
                    return  parameter;
                }
              
                return "#eaeaea";
            }
            catch (Exception)
            {
                return parameter;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
