﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSheetModel
{
   public class IndexInfo : INotifyPropertyChanged
    {

        /// <summary>
        /// 接头名称
        /// </summary>
        private string _indexName;
        public string IndexName
        {
            get { return _indexName; }
            set
            {
                if (_indexName != value)
                {
                    _indexName = value;
                    OnPropertyChanged("IndexName");
                }
            }
        }
        /// <summary>
        /// 接头七端
        /// </summary>
        private string _index7;
        public string Index7
        {
            get { return _index7; }
            set
            {
                if (_index7 != value)
                {
                    _index7 = value;
                    OnPropertyChanged("Index7");
                }
            }
        }
        /// <summary>
        ///七端顺序
        /// </summary>
        private string _p7Seq;
        public string P7Seq
        {
            get { return _p7Seq; }
            set
            {
                if (_p7Seq != value)
                {
                    _p7Seq = value;
                    OnPropertyChanged("P7Seq");
                }
            }
        }
        /// <summary>
        /// 接头5端
        /// </summary>
        private string _index5;
        public string Index5
        {
            get { return _index5; }
            set
            {
                if (_index5 != value)
                {
                    _index5 = value;
                    OnPropertyChanged("Index5");
                }
            }
        }
        /// <summary>
        ///5端顺序
        /// </summary>
        private string _p5Seq;
        public string P5Seq
        {
            get { return _p5Seq; }
            set
            {
                if (_p5Seq != value)
                {
                    _p5Seq = value;
                    OnPropertyChanged("P5Seq");
                }
            }
        }
        public ObservableCollection<IndexInfo> GetDictionaryByName(string Path, string dicName)
        {
            ObservableCollection<IndexInfo> indexInfos = new ObservableCollection<IndexInfo>();
            string[][] excelData = getExcelRows(Path,dicName);
            foreach (string[] row in excelData)
            {
                if (string.IsNullOrWhiteSpace(row[0])|| row[0].Contains("接头名称"))
                    continue;
                else
                {
                    IndexInfo indexInfo = new IndexInfo();
                    indexInfo._indexName = row[0];
                    indexInfo.Index7 = row[1];
                    indexInfo.P7Seq = row[2];
                    if (dicName == "double") {
                        indexInfo.Index5 = row[3];
                        indexInfo.P5Seq = row[4];
                    }
                    indexInfos.Add(indexInfo);
                }
            }
            return indexInfos;
        }
        private string[][] getExcelRows(string Path,string dicName)
        {
            string[][] row;
            using (ExcelProcessor excel = new ExcelProcessor())
            {
                int sheetNum = 1;
                switch (dicName.ToLower())
                {
                    case "single": sheetNum = 1; break;
                    case "double": sheetNum = 2; break;
                    default: sheetNum = 1; break;
                }
                row = excel.ParseExcelSheet(Path, sheetNum);
            }
            return row;

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
