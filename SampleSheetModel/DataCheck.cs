﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleSheetModel
{
    public class DataCheck
    {
        public bool CheckSampleNameLegil(ObservableCollection<SampleSheetData> sampleSheetDatas) {
            bool legil = true;
            legil = sampleSheetDatas.ToList().FindAll(x => x.IndexNameLegal == false).Count > 0;
            return legil;

        }
    }
}
