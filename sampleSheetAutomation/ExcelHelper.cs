﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using SampleSheetModel;
namespace sampleSheetAutomation
{
    class ExcelHelper:ExcelProcessor
    {
        public ObservableCollection<SampleSheetData> ImportSampleSheetData(String FilePath) {
            ObservableCollection<SampleSheetData> sampleSheetDatas = new ObservableCollection<SampleSheetData>();
            try
            {
               
                string[][] excelData = (new ExcelProcessor()).ParseExcelSheet(FilePath);
                string probeType = "";
                foreach (string[] row in excelData)
                {
                    if (string.IsNullOrWhiteSpace(row[3]) || string.IsNullOrWhiteSpace(row[4]) || row[3].Contains("样本序号"))
                        continue;
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(row[16])) {
                            probeType = row[16];
                        }
                        SampleSheetData sampleSheetData = new SampleSheetData();
                        sampleSheetData.SampleName = row[4];
                        sampleSheetData.IndexName = row[6];
                        sampleSheetData.ProbeTypes = probeType;
                        sampleSheetDatas.Add(sampleSheetData);
                    }
                }

            }
            catch (Exception ex) {

               

            }
            return sampleSheetDatas;
        }

        public bool ExportSampleSheetData(ReferenceInfo referenceInfo, ObservableCollection<SampleSheetData> sampleSheetDatas,string OutputFilePath) {
            int sheetNumber = 1;
            Workbook oWB = null;
            Sheets oSheets = null;
            Worksheet oSheet = null;
            Range oRange = null;
            string generPath = "";
            int count = 5;
            try
            {
                string path = System.Windows.Forms.Application.StartupPath;
                string templatPath ="";
                if (referenceInfo.operateBaseInfo.MarkTypes == "单")
                {
                    templatPath = path + @".\excel\sampleSheetTemplate.xlsx";
                }
                else {
                    templatPath = path + @".\excel\sampleSheetTemplate2.xlsx";
                }
                DateTime dateTime = DateTime.Now;
                generPath = OutputFilePath + "\\SampleSheet生成" + dateTime.Year + "-" + dateTime.Month + "-" + dateTime.Day + "-" + dateTime.Hour + "-" + dateTime.Minute + ".xlsx";
                File.Copy(templatPath, generPath);
                oWB = oWBs.Open(generPath);
                oSheets = oWB.Worksheets;
                oSheet = oSheets[sheetNumber];
                oRange = oSheet.UsedRange;
                oRange.get_Range("B3", "B3").Value2 = referenceInfo.operateBaseInfo.Operater;
                oRange.get_Range("B4", "B4").Value2 = referenceInfo.operateBaseInfo.OperateDate1+ referenceInfo.operateBaseInfo.KitType;

                oRange.get_Range("B5", "B5").Value2 =referenceInfo.operateBaseInfo.OperateDate != null ? Convert.ToDateTime(referenceInfo.operateBaseInfo.OperateDate).ToString("yyyy/MM/dd") : "";

                string[] describution = referenceInfo.operateBaseInfo.KitType.Split(' ');
                string descr = "";
                for (int i = 1; i < describution.Length; i++) {
                    descr += describution[i] + " ";
                }
                oRange.get_Range("B9", "B9").Value2 = descr.Trim() ;
                oRange.get_Range("A13", "A13").Value2 = referenceInfo.operateBaseInfo.ReadLength;
                oRange.get_Range("A14", "A14").Value2 = referenceInfo.operateBaseInfo.ReadLength;
                int startLine = 23;

                string preSampleId = referenceInfo.operateBaseInfo.OperateDate1; //dateTime.ToString("yyyyMMdd") + "-";
                IndexInfo indexInfo = new IndexInfo();
                List<IndexInfo> indexInfos;
                string templatPath1 = path + @".\excel\indexInfo.xlsx";
                if (referenceInfo.operateBaseInfo.MarkTypes == "单")
                {
                    indexInfos = indexInfo.GetDictionaryByName(templatPath1, "single").ToList();
                    //单标签Chemistry是Default，双标签是Amplicon
                    //单标签Assay是TruSeq LT，双标签是TruSeq HT
                    oRange.get_Range("B10", "B10").Value2 = "Default";
                    oRange.get_Range("B8", "B8").Value2 = "TruSeq LT";
                }
                else {
                    indexInfos = indexInfo.GetDictionaryByName(templatPath1, "double").ToList();
                    //单标签Chemistry是Default，双标签是Amplicon
                    oRange.get_Range("B10", "B10").Value2 = "Amplicon";
                    oRange.get_Range("B8", "B8").Value2 = "TruSeq HT";
                }
                if (referenceInfo.operateBaseInfo.KitType.Contains("Miseq"))
                {
                    oRange.get_Range("A16", "A16").Value2 = "[Settings]";
                    oRange.get_Range("A17", "A17").Value2 = "ReverseComplement";
                    oRange.get_Range("B17", "B17").Value2 = "0";
                    //miSeq是FASTQ Only，其他是NextSeq FASTQ Only
                    oRange.get_Range("B7", "B7").Value2 = "FASTQ Only";
                }
                else
                {
                    oRange.get_Range("A17", "A17").Value2 = "[Settings]";
                    oRange.get_Range("B7", "B7").Value2 = "NextSeq FASTQ Only";
                }

                foreach (SampleSheetData s in sampleSheetDatas) {

                    oRange.get_Range("A" + startLine, "A" + startLine).Value2 = preSampleId+"-"+(startLine-22).ToString();
                    oRange.get_Range("B" + startLine, "B" + startLine).Value2 = s.SampleName;
                    IndexInfo index = indexInfos.Find(x=>x.IndexName==s.IndexName);
                    oRange.get_Range("E" + startLine, "E" + startLine).Value2 = index.Index7;
                    oRange.get_Range("F" + startLine, "F" + startLine).Value2 = index.P7Seq;
                    if (referenceInfo.operateBaseInfo.MarkTypes == "双")
                    {
                        oRange.get_Range("G" + startLine, "G" + startLine).Value2 = index.Index5;
                        oRange.get_Range("H" + startLine, "H" + startLine).Value2 = index.P5Seq;
                        oRange.get_Range("J" + startLine, "J" + startLine).Value2 = s.ProbeTypes;
                    }
                    else {
                        oRange.get_Range("H" + startLine, "H" + startLine).Value2 = s.ProbeTypes;
                    }
                    startLine++;
                }
               // oRange.NumberFormatLocal = "0";
            }
            catch (Exception ex) {

            }
            finally
            {
              
                if (oWB != null)
                {   
                    oWB.Save();
                    //oWB.SaveAs(Missing.Value, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                    oWB.Close();
                }

                Marshal.ReleaseComObject(oSheet);
                Marshal.ReleaseComObject(oSheets);
                Marshal.ReleaseComObject(oWB);
                GC.Collect();
            }
            genertCSV(generPath);
            return false;
        }
        public void genertCSV(string path) {
   
            Workbook oWB = null;
            Sheets oSheets = null;
            Worksheet oSheet = null;
            Range oRange = null;
     
            oWB = oWBs.Open(path);
            oSheets = oWB.Worksheets;
            oSheet = oSheets[1];
            oRange = oSheet.UsedRange;
            string genePath = path.Replace(".xlsx", ".txt");
            oWB.SaveAs(genePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            oWB.Close();
            File.Delete(path);
            StreamReader sr = new StreamReader(genePath, Encoding.UTF8, false);
            string contenttxt = sr.ReadToEnd();
            sr.Close();
            StreamWriter sw = new StreamWriter(genePath.Replace(".txt", ".csv"), false, Encoding.UTF8);
            sw.Write(contenttxt);
            sw.Close();
            File.Delete(genePath);
        }
    }
}
