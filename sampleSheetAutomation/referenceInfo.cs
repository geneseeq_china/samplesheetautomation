﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SampleSheetModel;

namespace sampleSheetAutomation
{
    class ReferenceInfo : INotifyPropertyChanged
    {

        public OperateBaseInfo operateBaseInfo { get; set; }

        /// <summary>
        /// 试剂盒类型
        /// </summary>
        private ObservableCollection<BaseDic> _kitTypeList;
        public ObservableCollection<BaseDic> KitTypeList {
            get
            {
                return _kitTypeList;
            }
            set
            {
                if (_kitTypeList != value)
                {
                    _kitTypeList = value;
                    OnPropertyChanged("KitTypeList");
                }
            }

        }

        /// <summary>
        /// 标签类型
        /// </summary>
        private ObservableCollection<BaseDic>  _markTypeList;
        public ObservableCollection<BaseDic> MarkTypes
        {
            get
            {
                return _markTypeList;
            }
            set
            {
                if (_markTypeList != value)
                {
                    _markTypeList = value;
                    OnPropertyChanged("MarkTypes");
                }
            }

        }

        /// <summary>
        /// 测序读长
        /// </summary>
        private ObservableCollection<BaseDic> _readLengthList;
        public ObservableCollection<BaseDic> ReadLengthList
        {
            get
            {
                return _readLengthList;
            }
            set
            {
                if (_readLengthList != value)
                {
                    _readLengthList = value;
                    OnPropertyChanged("ReadLengthList");
                }
            }

        }

        /// <summary>
        /// 探针类型
        /// </summary>
        private ObservableCollection<BaseDic> _Probes;
        public ObservableCollection<BaseDic> Probes
        {
            get
            {
                return _Probes;
            }
            set
            {
                if (_Probes != value)
                {
                    _Probes = value;
                    OnPropertyChanged("Probes");
                }
            }
        }
        private ObservableCollection<SampleSheetData> _sampleSheetDatas = new ObservableCollection<SampleSheetData>();
        public ObservableCollection<SampleSheetData> SampleSheetDatas
        {
            get
            {
                return _sampleSheetDatas;
            }
            set
            {
                if (_sampleSheetDatas != value)
                {
                    _sampleSheetDatas = value;
                    OnPropertyChanged("SampleSheetDatas");
                }
            }
        }
        public ReferenceInfo()
        {
            BaseDic baseDic = new BaseDic();
            string path = System.Windows.Forms.Application.StartupPath;
            string templatPath = path + @".\excel\dictionary.xlsx";
            List<BaseDic> baseDics = baseDic.GetDictionars(templatPath);
            operateBaseInfo = new OperateBaseInfo();
            _kitTypeList = new ObservableCollection<BaseDic>(baseDics.Where(x => x.ItemType == "kittype"));
            _readLengthList = new ObservableCollection<BaseDic>(baseDics.Where(x => x.ItemType == "readlength"));
            _markTypeList = new ObservableCollection<BaseDic>(baseDics.Where(x => x.ItemType == "marktype"));
            _Probes = new ObservableCollection<BaseDic>(baseDics.Where(x => x.ItemType == "probetypes"));
            _sampleSheetDatas.Add(new SampleSheetData());
            _sampleSheetDatas.Add(new SampleSheetData());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
