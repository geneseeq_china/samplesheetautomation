﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SampleSheetModel
{
    public class SampleSheetData : INotifyPropertyChanged
    {
        private Regex regex = new Regex("^[a-zA-Z0-9-]+$");

        /// <summary>
        /// 样本名称
        /// </summary>
        private string _sampleName="";
        public string SampleName
        {
            get { return _sampleName; }
            set
            {
                if (_sampleName != value)
                {
                    _sampleName = value;
                    SampleNameLegal = regex.IsMatch(_sampleName);
                    SampleNameColor = "";
                    OnPropertyChanged("SampleName");
                }
            }
        }

        /// <summary>
        /// 输入名称是否合法
        /// </summary>
        private bool _sampleNameLegal=false;
        public bool SampleNameLegal
        {   
            
            get {
                return _sampleNameLegal;
            }
            set
            {
                if (_sampleNameLegal != value)
                {
                    _sampleNameLegal = value;
                    SampleNameColor = "";
                    OnPropertyChanged("SampleNameLegal");
                }
            }
        }


        /// <summary>
        /// 样本名称重复
        /// </summary>
        private bool _sampleNameRepeat=false;
        public bool SampleNameRepeat
        {
            get { return _sampleNameRepeat; }
            set
            {
                if (_sampleNameRepeat != value)
                {
                    _sampleNameRepeat = value;
                    SampleNameColor = "";
                    OnPropertyChanged("SampleNameRepeat");
                }
            }
        }
        /// <summary>
        /// 样本名称重复
        /// </summary>
        private string _sampleNameColor;
        public string SampleNameColor
        {
            get { return _sampleNameColor; }
            set
            {
                if (!_sampleNameLegal) {
                    _sampleNameColor = "#c00000";
                }else if(_sampleNameRepeat)
                {
                    _sampleNameColor = "#ffff00";
                }else{
                    _sampleNameColor = "#ffffff";
                }
                OnPropertyChanged("SampleNameColor");
            }
        }

        /// <summary>
        /// 接头名称
        /// </summary>
        private string _indexName;
        public string IndexName
        {
            get { return _indexName; }
            set
            {
                if (_indexName != value)
                {
                    _indexName = value;
                    OnPropertyChanged("IndexName");
                }
            }
        }
         
        /// <summary>
        /// index名称是否合法
        /// </summary>
        private bool _indexNameLegal;
        public bool IndexNameLegal
        {
            get { return _indexNameLegal; }
            set
            {
                if (_indexNameLegal != value)
                {
                    _indexNameLegal = value;
                    IndexNameLegalColor = "";
                    OnPropertyChanged("IndexNameLegal");
                }
            }
        }
        /// <summary>
        /// index名称是否合法
        /// </summary>
        private bool _indexNameExit =true;
        public bool IndexNameExit
        {
            get { return _indexNameExit; }
            set
            {
                if (_indexNameExit != value)
                {
                    _indexNameExit = value;
                    IndexNameLegalColor = "";
                    OnPropertyChanged("IndexNameExit");
                }
            }
        }
        /// <summary>
        /// index名称是否合法
        /// </summary>
        private string _indexNameLegalColor;
        public string IndexNameLegalColor
        {
            get { return _indexNameLegalColor; }
            set
            {
                    if (_indexNameLegal)
                    {
                        _indexNameLegalColor = "#ffff00";
                    }else if(!_indexNameExit) {
                       _indexNameLegalColor = "#77b1d6";
                    }
                    else {
                        _indexNameLegalColor = "#FFFFFF";
                    }
                    OnPropertyChanged("IndexNameLegalColor");
                
            }
        }

        /// <summary>
        /// 探针类型
        /// </summary>
        private string _probeTypes;
        public string ProbeTypes
        {
            get { return _probeTypes; }
            set
            {
                if (_probeTypes != value)
                {
                    _probeTypes = value;
                    OnPropertyChanged("ProbeTypes");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
