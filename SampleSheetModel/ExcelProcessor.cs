﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Collections.ObjectModel;
using System.IO;

namespace SampleSheetModel
{
   public class ExcelProcessor : IDisposable
    {
        protected Application oXL = null;
        protected Workbooks oWBs = null;

        public ExcelProcessor()
        {
            oXL = new Application();
            oXL.DisplayAlerts = false;
            oWBs = oXL.Workbooks;
        }


        protected double ConverStringToDouble(string text)
        {
            if (double.TryParse(text.Trim(), out double d))
            {
                return d;
            }
            else
            {
                return 0;
            }

        }
        protected int ConverStringToInt(string text)
        {
            if (int.TryParse(text.Trim(), out int d))
            {
                return d;
            }
            else
            {
                return 0;
            }

        }

        protected void MeargeCell(Range range, string start, string end, dynamic value)
        {
            Range merageRange = range.get_Range(start, end);
            merageRange.Merge();
            merageRange.Value2 = value;
        }
 
        public string[][] ParseExcelSheet(string excelPath, int sheetNumber = 1)
        {
            Workbook oWB = null;
            Sheets oSheets = null;
            Worksheet oSheet = null;
            Range oRange = null;

            try
            {
                oWB = oWBs.Open(excelPath, ReadOnly: true);
                oSheets = oWB.Worksheets;
                oSheet = oSheets[sheetNumber];
                oRange = oSheet.UsedRange;
                int numRows = oRange.Rows.Count;
                int numCols = oRange.Rows[1].Columns.Count;
                string[][] result = new string[numRows][];

                object[,] oRangeValues = (object[,])oRange.Value2;
                for (int i = 1; i <= oRange.Rows.Count; i++)
                {
                    string[] row = new string[numCols];
                    for (int j = 1; j <= numCols; j++)
                    {
                        row[j - 1] = Convert.ToString(oRangeValues[i, j]).Trim().Replace("\a", "").Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    }
                    result[i - 1] = row;
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (oWB != null)
                {
                    oWB.Save();
                    oWB.Close();
                }

                Marshal.ReleaseComObject(oSheet);
                Marshal.ReleaseComObject(oSheets);
                Marshal.ReleaseComObject(oWB);
                GC.Collect();
            }
        }
        public void Dispose()
        {
            if (oWBs != null)
            {
                oWBs.Close();
            }

            if (oXL != null)
                oXL.Quit();
            Marshal.ReleaseComObject(oWBs);
            Marshal.ReleaseComObject(oXL);
            GC.Collect();
        }


    }
}
